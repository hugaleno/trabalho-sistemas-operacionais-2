/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Random;

/**
 *
 * @author Hugaleno
 */
public class ListaEncadeada {

    private Nodo lista;

    /**
     * Construtor da classe Lista Encadeada
     */
    public ListaEncadeada() {
        lista = null;
    }

    /**
     * Verifica se a lista está vazia.
     *
     * @return true se vazia, false se não vazia;
     */
    public boolean estaVazia() {
        return lista == null;
    }

    /**
     * Verifica quantos elementos a fila possui.
     *
     * @return res Quantidade de elementos na lista.
     */
    public int numElem() {
        Nodo pa = lista;
        int res = 0;
        while (pa != null) {  //Percore a lista inteira.
            pa = pa.pro;
            res++;
        }
        return res;
    }

    /**
     * Insere um elemento no começo na lista.
     *
     * @param pag Elemento a ser inserido no começo da lista.
     */
    public void insereNoInicio(Pagina pag) {
        Nodo nn = new Nodo(pag);
        if (estaVazia()) {
            lista = nn;
        } else {
            nn.pro = lista;
            lista = nn;
        }

    }

    /**
     * Insere um elemento na lista na posicão especificada;
     *
     * @param pag Elemento a ser inserido na lista na posição especificada.
     * @param posicao Posicao onde o elemento será inserido na lista.
     */
    public void insereNaPosicao(Pagina pag, int posicao) {
        Nodo pa = lista;
        Nodo nn = new Nodo(pag);
        if (posicao == 0) {
            insereNoInicio(pag);
        }
        int i = 1;
        while (i < posicao) {
            pa = pa.pro;
            i++;
        }
        nn.pro = pa.pro;
        pa.pro = nn;
    }

    /**
     * Substitui um elemento da lista na posição especificada.
     *
     * @param pag Elemento a ser sobreposto na lista na posicao especificada.
     * @param posicao Posição da lista onde o elemento será substituido por pag.
     */
    public void substituiNaPosicao(Pagina pag, int posicao) {
        Nodo pa = lista;
        Nodo nn = new Nodo(pag);
        if (posicao == 0) {
            //   lista.pro.val.zeraCounter();
            nn.pro = lista.pro;
            lista = nn;
        } else {
            int i = 1;
            while (i < posicao) {
                pa = pa.pro;
                i++;
            }
            nn.pro = pa.pro.pro;
            pa.pro = nn;
        }
    }

    /**
     * Verifica se o elemento especificado está presente na lista.
     *
     * @param pag Elemento a ser buscado.
     * @return true se o elemento pag estiver presente na lista, false caso
     * contrário.
     */
    public boolean estaNaLista(Pagina pag) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pag)) {
                return true;
            }
            pa = pa.pro;
        }
        return false;
    }

    /**
     * Zera o bit R(Referenciado) de todas as paginas presente na lista;
     */
    public void zeresima() {
        Nodo pa = lista;
        while (pa != null) {
            pa.val.setReferenciado(false);
            pa = pa.pro;
        }
    }

    /**
     * Metodo executado na ocorrência de um Hit no algoritmo Mru;
     *
     * @param pag Elemento a ser tratado.
     */
    public void lruHit(Pagina pag) {
        Nodo pa = lista;

        int i = 0;
        while (pa != null) {
            if (pa.val.equals(pag)) {
                insereNoInicio(removeFromPos(i));
            }
            i++;
            pa = pa.pro;
        }
    }
    
    /**
     * Método executado quando ocorre um Miss no algoritmo Ótimo.
     *
     * @return Pagina removida.
     * @param pag Elemento a ser inserido na lista.
     * @param posicao Posicao do elemento na lista
     */
    public Pagina otimoPageFault(Pagina pag, int posicao) {
        Pagina paginaRemovida;
        if (posicao != -1) {
            paginaRemovida = removeFromPos(posicao);
        } else {
            paginaRemovida = removeDoFinal();
        }

        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Remove um elemento da lista de uma posicão especificada
     *
     * @param posicao Posição do elemento que deverá ser removido.
     * @return res Pagina removida.
     */
    public Pagina removeFromPos(int posicao) {
        Nodo pa = lista;
        Pagina res;
        if (posicao == 0) {
            res = removeDoInicio();
            return res;
        }
        int i = 1;
        while (i < posicao) {
            pa = pa.pro;
            i++;
        }
        res = pa.pro.val;
        pa.pro = pa.pro.pro;
        return res;
    }

    /**
     * Remove um elemento do começo da lista.
     *
     * @return aux Retorna o elemento removido.
     */
    public Pagina removeDoInicio() {
        Pagina aux;
        aux = lista.val;
        lista = lista.pro;
        return aux;

    }

    /**
     * Remove um elemento do final da lista.
     *
     * @return res Retorna o elemento removido.
     */
    public Pagina removeDoFinal() {
        Pagina res;
        Nodo pa = lista;
        while (pa.pro.pro != null) {
            pa = pa.pro;
        }
        res = (Pagina) pa.pro.val;
        pa.pro = null;
        return res;
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo Rand.
     *
     * @return Pagina removida.
     * @param pag Elemento a ser inserido na lista.
     * @param numFrames Numero de elementos da lista
     */
    public Pagina randPageFault(Pagina pag, int numFrames) {
        Random random = new Random();
        Pagina paginaRemovida = removeFromPos(random.nextInt(numFrames));
        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Pega a posição na lista do elemento desejado.
     *
     * @param pag Elemento que se deseja saber a posição na lista.
     * @return posicao Posição do elemento na lista.
     */
    public int getPosicao(Pagina pag) {
        int posicao = -1;
        Nodo pa = lista;
        while (pa != null) {
            posicao++;
            if (pa.val.equals(pag)) {
                return posicao;
            }
            pa = pa.pro;
        }

        return posicao;
    }

    public Pagina getDaPosicao(int posicao) {
        Nodo pa = lista;
        for (int i = 1; i <= posicao; i++) {
            pa = pa.pro;
        }
        return pa.val;
    }

    /**
     * Altera a propriedade "modificada" de um elemento especificado.
     *
     * @param pag Elemento que se deseja alterar a propriedade "modificada".
     */
    public void setModificada(Pagina pag) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pag)) {
                pa.val.setModificado(true);
            }
            pa = pa.pro;
        }
    }

    /**
     * Verifica se o elemento terá a sua propriedade modificada setada ou nao.
     *
     * @param pag Elemento que será analisado.
     */
    public void eModificada(Pagina pag) {
        if (pag.getTipoAcesso().equalsIgnoreCase("W")) {
            setModificada(pag);

        }

    }

    public boolean foiModificada(Pagina pagina) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pagina)) {
                return pa.val.getModificado();
            }
            pa = pa.pro;
        }
        return false;
    }

    public int getClasse(Pagina pagina) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pagina)) {
                return pa.val.getClasse();
            }
            pa = pa.pro;
        }
        return -1;
    }

    /**
     * Especifica do algoritmo NRU. Busca na lista qual elemento possui menor
     * classe.
     *
     * @return Retorna a posição desse elemento na lista.
     */
    public int getMenorClasse() {
        Nodo pa = lista;
        int menorClasse = pa.val.getClasse();
        Pagina res = pa.val;
        while (pa != null) {
            if (pa.val.getClasse() <= menorClasse) {
                menorClasse = pa.val.getClasse();
                res = pa.val;
            }
            pa = pa.pro;
        }

        return getPosicao(res);
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo NRU.
     *
     * @return Página removida.
     * @param pag Pagina que será adicionada a lista.
     */
    public Pagina nruPageFault(Pagina pag) {
        Pagina paginaRemovida = removeFromPos(getMenorClasse());
        insereNoInicio(pag);

        return paginaRemovida;
    }

    /**
     * Específica dos algoritmos LFU e MFU Incrementa a variavel counter que
     * registra quantos acessos a pagina obteve.
     *
     * @return Página que teve o contador incrementado.
     * @param pag Pagina que terá seu contador incrementado.
     */
    public Pagina incrementaAcessoPagina(Pagina pag) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pag)) {
                pa.val.incrementaCounter();
                return pa.val;
            }
            pa = pa.pro;
        }
        return null;
    }

    /**
     * Busca a página que obteve menos acessos dentro da lista.
     *
     * @return Retorna a posição da página menos acessada na lista.
     */
    public int menosAcessada() {
        Nodo pa = lista;
        int acessos = lista.val.getCounter();
        Pagina res = lista.val;
        while (pa != null) {
            if (pa.val.getCounter() <= acessos) {
                acessos = pa.val.getCounter();
                res = pa.val;
            }
            pa = pa.pro;
        }
        return getPosicao(res);
    }

    /**
     * Busca a página que obteve mais acessos dentro da lista.
     *
     * @return Retorna a posição da página mais acessada na lista.
     */
    public int maisAcessada() {
        Nodo pa = lista;
        int acessos = lista.val.getCounter();
        Pagina res = lista.val;
        while (pa != null) {
            if (pa.val.getCounter() >= acessos) {
                acessos = pa.val.getCounter();
                res = pa.val;
            }
            pa = pa.pro;
        }
        return getPosicao(res);
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo lfu.
     *
     * @return Pagina removida
     * @param pag Pagina que será adicionada a lista.
     */
    public Pagina lfuPageFault(Pagina pag) {
        Pagina paginaRemovida = removeFromPos(menosAcessada());
        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo mfu.
     *
     * @return Página removida.
     * @param pag Página que será adicionada a lista.
     */
    public Pagina mfuPageFault(Pagina pag) {
        Pagina paginaRemovida = removeFromPos(maisAcessada());
        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo mru.
     *
     * @return Pagina removida
     * @param pag Pagina que será adicionada a lista.
     */
    public Pagina lruPageFault(Pagina pag) {
        Pagina paginaRemovida = removeDoFinal();
        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo Segunda Chance.
     *
     * @return Pagina que foi removida
     * @param pag Pagina que será adicionada a lista.
     */
    public Pagina secondChancePageFault(Pagina pag) {
        Pagina paginaRemovida = secondChanceRemove();
        insereNoInicio(pag);
        return paginaRemovida;
    }

    /**
     * Método executado quando ocorre um Miss no algoritmo FIFO.
     *
     * @return pagina
     * @param pag Pagina que será adicionada a lista.
     */
    public Pagina fifoPageFault(Pagina pag) {
        Pagina paginaRemovida = fifoRemove();
        insereNoInicio(pag);

        return paginaRemovida;
    }

    /**
     * Método usado pelo algoritmo FIFO para remoção da pagina da lista.
     *
     * @return Retorna pagina removida
     */
    public Pagina fifoRemove() {
        Pagina res;
        Nodo pa = lista;
        while (pa.pro.pro != null) {
            pa = pa.pro;
        }
        res = (Pagina) pa.pro.val;
        pa.pro = null;
        return res;
    }

    /**
     * Seta a propriedade de referenciada da página especificada na lista.
     *
     * @param pg Pagina que se deseja alterar a propriedade referenciada.
     */
    public void setBitR(Pagina pg) {
        Nodo pa = lista;
        while (pa != null) {
            if (pa.val.equals(pg)) {
                pa.val.setReferenciado(true);
            }
            pa = pa.pro;
        }
    }

    /**
     * Método usado exclusivamente pelo algoritmo Segunda Chance para remoção da
     * pagina
     *
     * @return pagina removida
     */
    public Pagina secondChanceRemove() {
        Pagina res;
        Nodo pa;
        while (true) {
            pa = lista;
            while (pa.pro.pro != null) {
                pa = pa.pro;
            }

            if (pa.pro.val.getReferenciado()) {
                pa.pro.val.setReferenciado(false);
                res = (Pagina) pa.pro.val;
                pa.pro = null;
                insereNoInicio(res);

            } else {
                res = (Pagina) pa.pro.val;
                pa.pro = null;
                return res;
            }
        }

    }

    
}
