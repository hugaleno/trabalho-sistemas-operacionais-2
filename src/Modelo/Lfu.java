/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;

/**
 *
 * @author Hugaleno
 */
public class Lfu extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final GerarGrafico grafico;
    private int quantidadeDeFrames = 0;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;

    public Lfu(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames,GerarGrafico grafico) {
        this.grafico = grafico;
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            pagina.setCounter(0);
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina.clone());
                    
                //Hit, página se encontra na lista(memória). 
                } else {
                    lista.incrementaAcessoPagina(pagina);
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    lista.incrementaAcessoPagina(pagina);
                    numeroDeHits++;
                    
                //Miss, página não se encontra na lista(memória).    
                } else {                    
                    numeroDeMiss++;
                    lista.lfuPageFault(pagina.clone());
                }
            }
        }

        grafico.addLfu(quantidadeDeFrames, numeroDeHits);

    }
}
