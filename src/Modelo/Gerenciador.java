/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import subspagina.SubsPagina;

/**
 *
 * @author Hugaleno
 */
public class Gerenciador {

    public ArrayList<Pagina> lerArquivo(String path) {
        Pagina pg;
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader(path)).useDelimiter("-");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SubsPagina.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<Pagina> listaDePaginas = new ArrayList<>();
        if (sc != null) {
            while (sc.hasNext()) {
                pg = new Pagina(sc.next());
                listaDePaginas.add(pg);
            }
        }
        return listaDePaginas;
    }
}
