/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.*;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class NruInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final int tempoZeresima;
    private int tempoZeresimaDinamico;
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private boolean isPassoAPasso = true;
    private final InterfaceInterativa interfaceInterativa;
    private final Semaphore semaforo;

    public NruInterativo(ArrayList<Pagina> listaDePaginas,
            int quantidadeDeFrames, int tempo, InterfaceInterativa interfaceInterativa) {

        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.tempoZeresima = tempo;
        this.tempoZeresimaDinamico = tempo;
        this.interfaceInterativa = interfaceInterativa;
        this.semaforo = new Semaphore(0);

    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {

            String nomePagina = pagina.toString();

            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SegundaChanceInterativo.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

            pagina.setReferenciado(true);
            pagina.setModificado(false);

            //Simulação da zerésima do sistema operacional. Quando o número de páginas
            //acessadas chega a tempoZeresima, o bit R de todas as páginas da memória
            //são zerados.
            if (tempoZeresimaDinamico == 0) {
                lista.zeresima();
                logZeresima();
                for (int i = 0; i < lista.numElem(); i++) {
                    int classePagina = lista.getDaPosicao(i).getClasse();
                    String nomePaginaFrame = lista.getDaPosicao(i).toString();
                    addInfoOnHit(nomePaginaFrame, lista.getDaPosicao(i)
                            .getNumero().toString(), classePagina);
                }

                tempoZeresimaDinamico = tempoZeresima;
            }
            tempoZeresimaDinamico--;

            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    nruLog(Mediador.MISS, nomePagina,0);
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina.clone());
                    lista.eModificada(pagina);
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);

                    nruLog(Mediador.PAG_REFERENCIADA, nomePagina,0);
                    
                    //Tipo de acesso W simula a ocorrência de alguma operação 
                    //de escrita na página
                    if (pagina.getTipoAcesso().equalsIgnoreCase("W")) {
                        nruLog(Mediador.PAG_MODIFICADA, nomePagina,0);

                    } else {
                        nruLog(Mediador.PAG_NAO_MODIFICADA, nomePagina,0);

                    }

                    int classePagina = lista.getClasse(pagina);
                    addInfoOnMiss(nomePagina, classePagina);

                    //Hit, página se encontra na lista(memória).            
                } else {
                    lista.eModificada(pagina);
                    nruLog(Mediador.HIT, nomePagina,0);
                    numeroDeHits++;
                    lista.setBitR(pagina);
                    nruLog(Mediador.PAG_REFERENCIADA, nomePagina,0);

                    if (pagina.getTipoAcesso().equalsIgnoreCase("W")) {
                        nruLog(Mediador.PAG_MODIFICADA, nomePagina,0);

                    } else {
                        if (lista.foiModificada(pagina)) {
                            interfaceInterativa.adicionarInfo("Página " + nomePagina
                                    + " já foi modificada anteriormente.\n");
                        } else {
                            nruLog(Mediador.PAG_NAO_MODIFICADA, nomePagina,0);
                        }
                    }
                    int classePagina = lista.getClasse(pagina);

                    addInfoOnHit(nomePagina, pagina.getNumero().toString(), classePagina);
                }

                //Memória cheia, procedimento de substituição de página necessário para cada miss.    
            } else {
                //Hit, página se encontra na lista(memória).
                if (lista.estaNaLista(pagina)) {
                    lista.eModificada(pagina);
                    nruLog(Mediador.HIT, nomePagina,0);
                    numeroDeHits++;
                    lista.setBitR(pagina);
                    nruLog(Mediador.PAG_REFERENCIADA, nomePagina,0);

                    if (pagina.getTipoAcesso().equalsIgnoreCase("W")) {
                        nruLog(Mediador.PAG_MODIFICADA, nomePagina,0);
                    } else {
                        if (lista.foiModificada(pagina)) {
                            interfaceInterativa.adicionarInfo("Página " + nomePagina
                                    + " já foi modificada anteriormente.\n");
                        } else {
                            nruLog(Mediador.PAG_NAO_MODIFICADA, nomePagina,0);
                        }
                    }
                    int classePagina = lista.getClasse(pagina);
                    addInfoOnHit(nomePagina, 
                            pagina.getNumero().toString(), 
                            classePagina);

                    //Miss, página não se encontra na lista(memória).
                } else {
                    nruLog(Mediador.MISS, nomePagina, 0);
                    numeroDeMiss++;
                    Pagina paginaRemovida = lista.nruPageFault(pagina.clone());
                    lista.eModificada(pagina);
                    nruLog(Mediador.REMOVIDA, paginaRemovida.toString(), paginaRemovida.getClasse());
                    interfaceInterativa.removePaginaFramePorNome(
                            paginaRemovida.getNumero().toString());

                    nruLog(Mediador.PAG_REFERENCIADA, nomePagina, 0);

                    if (pagina.getTipoAcesso().equalsIgnoreCase("W")) {
                        nruLog(Mediador.PAG_MODIFICADA, nomePagina, 0);
                    } else {
                        nruLog(Mediador.PAG_NAO_MODIFICADA, nomePagina, 0);
                    }

                    int classePagina = lista.getClasse(pagina);
                    addInfoOnMiss(nomePagina, classePagina);

                }
            }
            interfaceInterativa.removeDaFila();
        }

        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();

    }

    private void addInfoOnMiss(String nomePagina, int classePagina) {
        interfaceInterativa.adicionarInfo("Página " + nomePagina
                + " adicionada na memória. Classe: " + classePagina + ".\n");
        interfaceInterativa.inserirPaginaFrame(nomePagina
                + "- Classe: " + classePagina);
    }

    private void addInfoOnHit(String nomePagina, String numeroDaPagina, int classePagina) {
        interfaceInterativa.adicionarInfo("Página " + nomePagina
                + ". Classe: " + classePagina + ".\n");
        interfaceInterativa.setPaginaFrameTexto(numeroDaPagina,
                nomePagina + "- Classe: " + classePagina);
    }

    private void nruLog(int tipo, String nomePagina, int classePagina) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina + 
                        " já se encontra na memória.\n");
                break;

            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina + 
                        " não se encontra na memória.\n");
                break;
                
            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + 
                            nomePagina  
                            + " removida da memória pois possui a menor classe."
                            + " Classe: "+ classePagina + ".\n");
                break;
               
            case Mediador.PAG_REFERENCIADA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina + 
                        " marcada como referenciada.\n");
                break;

            case Mediador.PAG_MODIFICADA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina + 
                        " marcada como modificada.\n");
                break;

            case Mediador.PAG_NAO_MODIFICADA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina + 
                        " não modificada.\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

    private void logZeresima() {
        interfaceInterativa.adicionarInfo("Bit de referência zerado para"
                + " todas as páginas.\n "
                + "Algumas classes tiveram suas"
                + " classes alteradas devido a zerésima. "
                + "Segue abaixo as classes atualizadas.\n");

    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }
}
