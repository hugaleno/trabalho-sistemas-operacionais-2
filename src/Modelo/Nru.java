/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;

/**
 *
 * @author Hugaleno
 */
public class Nru extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final GerarGrafico grafico;
    private final int tempoZeresima;
    private int tempoZeresimaDinamico;
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;

    public Nru(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames, int tempo,GerarGrafico grafico) {
        this.grafico = grafico;
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.tempoZeresima = tempo;
        this.tempoZeresimaDinamico = tempo;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            pagina.setReferenciado(true);
            pagina.setModificado(false);
            
            //Simulação da zerésima do sistema operacional. Quando o número de páginas
            //acessadas chega a tempoZeresima, o bit R de todas as páginas da memória
            //são zerados.
            if (tempoZeresimaDinamico == 0) {
                lista.zeresima();
                tempoZeresimaDinamico = tempoZeresima;
            }
            tempoZeresimaDinamico--;
            
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina.clone());
                    
                //Hit, página se encontra na lista(memória). 
                } else {
                    lista.setBitR(pagina);
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;
                    lista.setBitR(pagina);
                    
                //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    lista.nruPageFault(pagina.clone());
                }
            }
            lista.eModificada(pagina);
        }

         grafico.addNru(quantidadeDeFrames, numeroDeHits);

    }
}
