/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.ListaEncadeada;
import Modelo.Pagina;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class FifoInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private List<Pagina> paginas = new ArrayList<>();
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private boolean isPassoAPasso;
    private final Semaphore semaforo;
    private final InterfaceInterativa interfaceInterativa;

    public FifoInterativo(List<Pagina> listaDePaginas, int quantidadeDeFrames, InterfaceInterativa interfaceInterativa) {
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.interfaceInterativa = interfaceInterativa;
        this.isPassoAPasso = true;
        semaforo = new Semaphore(0);
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            String nomePagina = pagina.getNumero().toString();
            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FifoInterativo.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    fifoLog(Mediador.MISS, nomePagina);
                    lista.insereNoInicio(pagina);
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    fifoLog(Mediador.INSERIDA, nomePagina);

                    //Hit, página se encontra na lista(memória). 
                } else {
                    fifoLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;

                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    fifoLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;

                    //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    fifoLog(Mediador.MISS, nomePagina);
                    Pagina paginaRemovida = lista.fifoPageFault(pagina);
                    fifoLog(Mediador.REMOVIDA, paginaRemovida.toString());
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    fifoLog(Mediador.INSERIDA, nomePagina);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);

                }
            }

            interfaceInterativa.removeDaFila();
        }
        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();
    }

    private void fifoLog(int tipo, String nomePagina) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina
                        + " já se encontra na memória.\n");
                break;
            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina
                        + " não se encontra na memória.\n");
                break;
            case Mediador.INSERIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " adicionada na memória.\n");
                break;

            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina + 
                        " removida da memória.\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

}
