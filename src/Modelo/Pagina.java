/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Hugaleno
 */
public class Pagina {

    private Integer numero;
    private String tipoAcesso;
    private int counter = 0;
    private boolean referenciado;
    private boolean modificado;

    public Pagina() {
    }

    
    
    public Pagina(String str) {
        setPageValues(str);
        this.referenciado = true;
        this.modificado = false;
    }

    public int getClasse() {

        if (!getReferenciado() && !getModificado()) {
            return 0;
        }
        if (!getReferenciado() && getModificado()) {
            return 1;
        }
        if (getReferenciado() && !getModificado()) {
            return 2;
        }
        return 3;
    }

    /*
    *Separa o número da página do tipo de acesso e seta esses valores.
    */
    private void setPageValues(String str) {
        int i = 0;
        int tam = 0;
        while (i < str.length()) {
            if (str.charAt(i) != 'R' && str.charAt(i) != 'W') {
                tam++;
            }
            i++;
        }

        this.setNumero(Integer.parseInt(str.substring(0, tam)));
        this.setTipoAcesso(str.substring(tam));
    }

    public Integer getNumero() {
        return numero;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int numero) {
        this.counter = numero;
    }

    public void zeraCounter() {
        this.counter = 0;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public void incrementaCounter() {
        counter++;
    }

    public String getTipoAcesso() {
        return tipoAcesso;
    }

    public void setTipoAcesso(String tipoAcesso) {
        this.tipoAcesso = tipoAcesso;
    }

    public boolean getReferenciado() {
        return this.referenciado;
    }

    public void setReferenciado(boolean bu) {
        this.referenciado = bu;
    }

    public boolean getModificado() {
        return modificado;
    }

    public void setModificado(boolean bu) {
        modificado = bu;
    }

    @Override
    public String toString() {
        return getNumero().toString() + getTipoAcesso();
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numero == null) ? 0 : numero.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Pagina other = (Pagina) obj;
        if (numero == null) {
            if (other.numero != null) {
                return false;
            }
        } else if (!numero.equals(other.numero)) {
            return false;
        }
        return true;
    }

    @Override
    public Pagina clone(){
        Pagina pagina = new Pagina();
        pagina.setNumero(numero);
        pagina.setTipoAcesso(tipoAcesso);
        pagina.setReferenciado(referenciado);
        pagina.setModificado(modificado);
        pagina.setCounter(counter);
        return pagina;
    }

   
    
    

}
