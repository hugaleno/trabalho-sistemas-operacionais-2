/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.Dicas;
import Modelo.Fifo;
import Modelo.Gerenciador;
import Modelo.Interativos.FifoInterativo;
import Modelo.Interativos.LfuInterativo;
import Modelo.Interativos.MfuInterativo;
import Modelo.Interativos.LruInterativo;
import Modelo.Interativos.NruInterativo;
import Modelo.Interativos.OtimoInterativo;
import Modelo.Interativos.RandInterativo;
import Modelo.Interativos.SegundaChanceInterativo;
import Modelo.Lfu;
import Modelo.Mfu;
import Modelo.Lru;
import Modelo.Nru;
import Modelo.Otimo;
import Modelo.Pagina;
import Modelo.Rand;
import Modelo.SegundaChance;
import Visao.GerarGrafico;
import Visao.Home;
import Visao.InterfaceInterativa;
import java.awt.Point;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Hugaleno
 */
public class Mediador {

    public static final String FIFO = "FIFO";
    public static final String SEGUNDA_CHANCE = "Segunda Chance";
    public static final String LFU = "LFU";
    public static final String MFU = "MFU";
    public static final String RAND = "Rand";
    public static final String LRU = "LRU";
    public static final String NRU = "NRU";
    public static final String OTIMO = "Otimo";
    
    public static final int HIT = 0;
    public static final int MISS = 1;
    public static final int INSERIDA = 2;
    public static final int LRUHIT = 3;
    public static final int PAG_REFERENCIADA = 4;
    public static final int PAG_MODIFICADA = 5;
    public static final int PAG_NAO_MODIFICADA = 6;
    public static final int REMOVIDA = 7;
    
    private ArrayList<Pagina> paginas;
    private ArrayList<Pagina> paginasInterfaceInterativa;
    private final Home home;
    private final InterfaceInterativa interfaceInterativa;
    private final Gerenciador gerente;
    private GerarGrafico grafico;
    private Interativo algoritmoSelecionado;

    public Mediador(Home home, InterfaceInterativa novaInterface) {
        this.home = home;
        gerente = new Gerenciador();
        this.interfaceInterativa = novaInterface;
        home.addProcurarAction(new HomeActionsPerformed());
        home.addCboxsActions(new CboxsActionPerformed());
        home.addSpnAction(new SpnActionPerformed());
        home.setVisible(true);

        grafico = new GerarGrafico(home);
        //    grafico.setVisible(true);
    }

    private class HomeActionsPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == home.getBtnProcurarArquivo()) {
                JFileChooser procurar = new JFileChooser();

                int returnVal = procurar.showOpenDialog(home);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    home.setNomeArquivo(procurar.getSelectedFile().getName());
                    paginas = new ArrayList<>();
                    paginas = gerente.lerArquivo(procurar.getSelectedFile().getPath());
                }
            } else if (e.getSource() == home.getBtnJanelaInterativa()) {
                interfaceInterativa.setAdicionarActionPerformed(
                        new InterfaceInterativaActionsPerformed());
                
                interfaceInterativa.setSpinnerChangeListener(new SpnChangeListener());
                interfaceInterativa.setVisible(true);
            }

        }

    }

    /*
     * Cria uma página aleatória
     *
     */
    private Pagina criaPagina() {
        final String letras = "RW";
        final String numeros = "0123456789";
        Random rand = new Random();
        String nomePagina = "";
        nomePagina += numeros.charAt(rand.nextInt(10)) + "" + letras.charAt(rand.nextInt(2));
        Pagina pagina = new Pagina(nomePagina);
        return pagina;
    }
//Spin da nova interface

    private class SpnChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent ce) {
            interfaceInterativa.setTableNumeroEFrames(Integer.parseInt(
                    interfaceInterativa.getSpnNumeroDeFrames()
                    .getValue().toString()));
        }

    }

    private class InterfaceInterativaActionsPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
/*********************************Botão adicionar nova página*****************************************************/            
            if (e.getSource() == interfaceInterativa.getBtnAdicionarNovaPagina()) {
                if (interfaceInterativa.getTamFila() == 0) {
                    paginasInterfaceInterativa = new ArrayList<>();
                }
                Pagina pg = criaPagina();
                paginasInterfaceInterativa.add(pg);
                interfaceInterativa.addToFila(pg.toString());

/*********************************Botão Simular******************************************************************/
            } else if (e.getSource() == interfaceInterativa.getBtnSimular()) {
                algoritmoSelecionado.setIsPassoAPasso(false);
                algoritmoSelecionado.liberaPasso();

/*********************************Botão simular passo a passo*****************************************************/
            } else if (e.getSource() == interfaceInterativa.getBtnSimularPassoAPasso()) {
                algoritmoSelecionado.liberaPasso();

/*********************************Combobox Selecionar algoritmo**************************************************/
                
                
/*********************************Checa campos obrigatorios vazios e algoritmo selecionado***********************/                
            } else if (e.getSource() == interfaceInterativa.getCboxSelecionarAlgoritmo()) {
                if (interfaceInterativa.camposVaziosSemZeresima()
                        && !interfaceInterativa.getCboxSelecionarAlgoritmo()
                            .getSelectedItem().toString().equalsIgnoreCase("Selecionar")) {
                    JOptionPane.showMessageDialog(interfaceInterativa,
                            "Número de frames " +
                            "deve ser maior que zero/ Número de páginas na fila " +
                            "deve ser maior que zero", "Atenção", JOptionPane.WARNING_MESSAGE);
                    interfaceInterativa.getCboxSelecionarAlgoritmo().setSelectedIndex(0);
                    interfaceInterativa.getSpnNumeroDeFrames().requestFocus();

/*********************************Campos obrigatorios preenchidos e algoritmo selecionado***********************/
                } else {
/*********************************Verifica o algoritmo selecionado**********************************************/
                    String algoritmo = interfaceInterativa.getCboxSelecionarAlgoritmo()
                            .getSelectedItem().toString();
                    int quantidadeDeFrames = Integer.parseInt(interfaceInterativa
                            .getSpnNumeroDeFrames()
                            .getValue().toString());

/*********************************Algoritmo FIFO****************************************************************/
                    if (algoritmo.equalsIgnoreCase(FIFO)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(FIFO, Dicas.FIFO);
                        algoritmoSelecionado = new FifoInterativo(paginasInterfaceInterativa,
                                quantidadeDeFrames, interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();
                        
/*********************************Algoritmo Segunda chance******************************************************/
                    } else if (algoritmo.equalsIgnoreCase(SEGUNDA_CHANCE)) {
/*********************************Verifica campo obrigatório para este algoritmo********************************/ 
                        
                        if (interfaceInterativa.camposVaziosComZeresima()) {
                            JOptionPane.showMessageDialog(interfaceInterativa,
                                    "Valor zerésima não informado.",
                                    "Atenção", JOptionPane.WARNING_MESSAGE);
                            interfaceInterativa.getCboxSelecionarAlgoritmo().setSelectedIndex(0);
                            interfaceInterativa.getTxtZeresima().requestFocus();
                        } else {
/*********************Altera interface para o algoritmo selecionado e inicia a thread***************************/                            
                            limpaInterface();
                            interfaceInterativa.alteraDicas(SEGUNDA_CHANCE, Dicas.SEGUNDACHANCE);
                            algoritmoSelecionado = new SegundaChanceInterativo(
                                    paginasInterfaceInterativa,
                                    quantidadeDeFrames,
                                    interfaceInterativa.getZeresima(),
                                    interfaceInterativa);
                            
                            ((Thread) algoritmoSelecionado).start();
                        }
/*********************************Algoritmo LFU*****************************************************************/                        
                    } else if (algoritmo.equalsIgnoreCase(LFU)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(LFU, Dicas.LFU);
                        algoritmoSelecionado = new LfuInterativo(
                                paginasInterfaceInterativa,
                                quantidadeDeFrames,
                                interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();

/*********************************Algoritmo MFU****************************************************************/                        
                    } else if (algoritmo.equalsIgnoreCase(MFU)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(MFU, Dicas.MFU);
                        algoritmoSelecionado = new MfuInterativo(
                                paginasInterfaceInterativa,
                                quantidadeDeFrames,
                                interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();
                        
/*********************************Algoritmo RAND***************************************************************/
                    } else if (algoritmo.equalsIgnoreCase(RAND)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(RAND, Dicas.RAND);
                        algoritmoSelecionado = new RandInterativo(
                                paginasInterfaceInterativa,
                                quantidadeDeFrames,
                                interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();
                        
/*********************************Algoritmo LRU****************************************************************/
                    } else if (algoritmo.equalsIgnoreCase(LRU)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(LRU, Dicas.LRU);
                        algoritmoSelecionado = new LruInterativo(
                                paginasInterfaceInterativa,
                                quantidadeDeFrames,
                                interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();
                        
/*********************************Algoritmo NRU****************************************************************/
                    } else if (algoritmo.equalsIgnoreCase(NRU)) {
/*********************************Verifica campo obrigatório para este algoritmo*******************************/                        
                        if (interfaceInterativa.camposVaziosComZeresima()) {
                            JOptionPane.showMessageDialog(
                                    interfaceInterativa,
                                    "Valor zerésima não informado.",
                                    "Atenção", JOptionPane.WARNING_MESSAGE);
                            interfaceInterativa.getCboxSelecionarAlgoritmo().setSelectedIndex(0);
                            interfaceInterativa.getTxtZeresima().requestFocus();
                        } else {
                            limpaInterface();
                            interfaceInterativa.alteraDicas(NRU, Dicas.NRU);
                            algoritmoSelecionado = new NruInterativo(
                                    paginasInterfaceInterativa,
                                    quantidadeDeFrames,
                                    interfaceInterativa.getZeresima(),
                                    interfaceInterativa);
                            ((Thread) algoritmoSelecionado).start();
                        }
                        
/*********************************Algoritmo OTIMO*************************************************************/
                    } else if (algoritmo.equalsIgnoreCase(OTIMO)) {
                        limpaInterface();
                        interfaceInterativa.alteraDicas(OTIMO, Dicas.OTIMO);
                        algoritmoSelecionado = new OtimoInterativo(
                                paginasInterfaceInterativa,
                                quantidadeDeFrames,
                                interfaceInterativa);
                        ((Thread) algoritmoSelecionado).start();
                    }
                }
            }

        }

    }

    private void limpaInterface() {
        interfaceInterativa.limparTxtArea();
        bloqueiaItens(false);
    }

    protected void bloqueiaItens(boolean bol) {
        interfaceInterativa.getBtnAdicionarNovaPagina().setEnabled(bol);
        interfaceInterativa.getSpnNumeroDeFrames().setEnabled(bol);
        interfaceInterativa.getTxtBitReferencia().setEnabled(bol);
        interfaceInterativa.getCboxSelecionarAlgoritmo().setEnabled(bol);

    }

    private class SpnActionPerformed implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {

            Point point = grafico.getLocation();
            grafico.dispose();

            grafico = new GerarGrafico(home);
            grafico.setLocation(point);
            grafico.setVisible(true);
        }

    }

    private class CboxsActionPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!grafico.isVisible()) {
                grafico.setVisible(true);
            } else {
                grafico.requestFocus();
            }
            
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbFifo()) {
                if (home.getCbFifo().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Fifo fifo = new Fifo(paginas, i, grafico);
                        fifo.start();

                    }

                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbLfu()) {
                if (home.getCbLfu().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Lfu lfu = new Lfu(paginas, i, grafico);
                        lfu.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbMfu()) {
                if (home.getCbMfu().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Mfu mfu = new Mfu(paginas, i, grafico);
                        mfu.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbMru()) {
                if (home.getCbMru().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Lru mru = new Lru(paginas, i, grafico);
                        mru.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbNur()) {
                if (home.getCbNur().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Nru nru = new Nru(paginas, i, home.getTempoZerar(), grafico);
                        nru.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbOtimo()) {
                if (home.getCbOtimo().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Otimo otimo = new Otimo(paginas, i, grafico);
                        otimo.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbRand()) {
                if (home.getCbRand().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        Rand rand = new Rand(paginas, i, grafico);
                        rand.start();

                    }
                }
            }
            //Iniciar uma thread para cada numero de frames do intervalo.
            if (e.getSource() == home.getCbSegundaChance()) {
                if (home.getCbSegundaChance().isSelected()) {
                    int numFrameInicial = home.getNumFrameInicial();
                    int numFrameFinal = home.getNumFrameFinal();
                    for (int i = numFrameInicial; i <= numFrameFinal; i++) {
                        SegundaChance sc = new SegundaChance(paginas, i, home.getTempoZerar(), grafico);
                        sc.start();

                    }
                }
            }

        }

    }

    public interface Interativo {

        public void liberaPasso();

        public void setIsPassoAPasso(boolean isPassoAPasso);

    }

}
