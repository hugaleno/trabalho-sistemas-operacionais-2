/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.*;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class LfuInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private int quantidadeDeFrames = 0;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private boolean isPassoAPasso = true;
    private final Semaphore semaforo;
    private final InterfaceInterativa interfaceInterativa;

    public LfuInterativo(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames, InterfaceInterativa interfaceInterativa) {
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.semaforo = new Semaphore(0);
        this.interfaceInterativa = interfaceInterativa;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            pagina.setCounter(0);

            String nomePagina = pagina.getNumero().toString();
            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(LfuInterativo.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lfuLog(Mediador.MISS, nomePagina, 0);
                    lista.insereNoInicio(pagina.clone());
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    lfuLog(Mediador.INSERIDA, nomePagina, 0);

                    //Hit, página se encontra na lista(memória).         
                } else {
                    Pagina paginaIncrementada = lista.incrementaAcessoPagina(pagina);
                    lfuLog(Mediador.HIT, nomePagina, paginaIncrementada.getCounter());
                    numeroDeHits++;

                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    Pagina paginaIncrementada = lista.incrementaAcessoPagina(pagina);
                    lfuLog(Mediador.HIT, nomePagina, paginaIncrementada.getCounter());
                    numeroDeHits++;

                    //Miss, página não se encontra na lista(memória).
                } else {
                    lfuLog(Mediador.MISS, nomePagina, 0);
                    numeroDeMiss++;
                    Pagina paginaRemovida = lista.lfuPageFault(pagina.clone());
                    lfuLog(Mediador.REMOVIDA, paginaRemovida.toString(),
                            paginaRemovida.getCounter());

                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    lfuLog(Mediador.INSERIDA, nomePagina, 0);
                }
            }
            interfaceInterativa.removeDaFila();
        }

        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();

    }

    private void lfuLog(int tipo, String nomePagina, int paginaCounter) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina
                        + " já se encontra na memória.\n"
                        + "Contador de acesso da página "
                        + nomePagina
                        + " incrementado. Valor atual: " + paginaCounter + "\n");
                break;
            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina
                        + " não se encontra na memória.\n");
                break;
            case Mediador.INSERIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " adicionada na memória.\n");
                break;

            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " removida da memória pois "
                        + "possui menor contador. Valor atual:"
                        + paginaCounter + ".\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }
}
