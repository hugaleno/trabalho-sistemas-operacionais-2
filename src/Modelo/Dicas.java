/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author hugaleno
 */
public class Dicas {
    public static final String FIFO = "Lembre-se, no FIFO:"
                            + "\n 1-Primeira página a entrar na memória"
                            + "\n é a primeira a sair." 
                            + "\n 2-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 3-Páginas são sempre removidas do "
                            + "\n final"
                            + "\n 4-Não interessa para o FIFO o tipo de "
                            + "\n acesso a página, leitura ou escrita";
    
    public static final String SEGUNDACHANCE = "Lembre-se, no Segunda Chance:"
                            + "\n 1-Primeira página a entrar na memória"
                            + "\n é a primeira a sair." 
                            + "\n 2-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 3-Páginas são sempre removidas do "
                            + "\n final, desde que a página não tenha"
                            + "\n sido referenciada recentemente"
                            + "\n 4-Não interessa para o Segunda Chance"
                            + "\n o tipo de acesso a página, leitura ou"
                            + "\n escrita."
                            + "\n 5-O bit de Referencia de todas as"
                            + "\n páginas são zerados periodicamente de"
                            + "\n acordo com o parâmetro zerésima.";
    
    public static final String LFU = "Lembre-se, no LFU:"
                            + "\n 1-Cada página possui um contador de"
                            + "\n acesso que é usado pelo algoritmo"
                            + "\n para decidir qual página será removida"
                            + "\n da memória."
                            + "\n 2-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 3-A página a ser removida é a que tiver"
                            + "\n menor contador de acesso."
                            + "\n 4-Não interessa para o LFU o tipo de"
                            + "\n acesso a página, leitura ou escrita";
    
    public static final String MFU = "Lembre-se, no MFU:"
                            + "\n 1-Cada página possui um contador de"
                            + "\n acesso que é usado pelo algoritmo"
                            + "\n para decidir qual página será removida"
                            + "\n da memória."
                            + "\n 2-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 3-A página a ser removida é a que tiver"
                            + "\n maior contador de acesso."
                            + "\n 4-Não interessa para o MFU o tipo de"
                            + "\n acesso a página, leitura ou escrita";
    
    public static final String RAND = "Lembre-se, no Rand:"
                            + "\n 1-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 2-A página a ser removida é escolhida"
                            + "\n de forma aleatória."
                            + "\n 3-Não interessa para o Rand o tipo de"
                            + "\n acesso a página, leitura ou escrita";
    
    public static final String LRU = "Lembre-se, no LRU:"
                            + "\n 1-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 2-A página a ser removida é a última"
                            + "\n da fila."
                            + "\n 3-A fila é reorganizada a cada novo "
                            + "\n acesso de forma que as páginas menos "
                            + "\n usadas estarão sempre no final da "
                            + "\n memória."
                            + "\n 4-Não interessa para o LRU o tipo de"
                            + "\n acesso a página, leitura ou escrita";
    public static final String NRU = "Lembre-se, no NRU:"
                            + "\n 1-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 2-A página a ser removida é a que"
                            + "\n possui a menor classe. Em caso de"
                            + "\n de empate, o FIFO é levado em conta"
                            + "\n para tomada de decisão."
                            + "\n 3-O NRU usa as informações de acesso,"
                            + "\n leitura(R) ou escrita(W) como parâmetros"
                            + "\n para decidir a classe das páginas."
                            + "\n As classes são as seguintes: "
                            + "\n 0- Não referenciada, não modificada "
                            + "\n 1- Não referenciada, modificada"
                            + "\n 2- Referenciada, não modificada"
                            + "\n 3- Referenciada, modificada"                        
                            + "\n 5-O bit de Referencia de todas as"
                            + "\n páginas são zerados periodicamente de"
                            + "\n acordo com o parâmetro zerésima.";
    
     public static final String OTIMO = "Lembre-se, no OTIMO:"
                            + "\n 1-Novas páginas são inseridas sempre "
                            + "\n no início."
                            + "\n 2-A escolha da página a ser removida"
                            + "\n leva em conta a posição das páginas,"
                            + "\n que estão atualmente carregadas na"
                            + "\n na memória, na fila de páginas. A "
                            + "\n página que estiver mais distante"
                            + "\n será a candidata a remoção. Em caso"
                            + "\n de empate a primeira página mais "
                            + "\n distante será removida."
                            + "\n 3-O algoritmo ótimo não é implementável"
                            + "\n porque precisa saber quais páginas"
                            + "\n ainda serão acessadas, o que na prática"
                            + "\n é impossível de se prever. No entanto,"
                            + "\n ele serve como parâmetro de comparação"
                            + "\n para os outros algoritmos."
                            + "\n 4-Não interessa para o Otimo o tipo de"
                            + "\n acesso a página, leitura ou escrita";
}
