/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.*;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class RandInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private boolean isPassoAPasso = true;
    private final InterfaceInterativa interfaceInterativa;
    private final Semaphore semaforo;

    public RandInterativo(ArrayList<Pagina> listaDePaginas,
            int quantidadeDeFrames, InterfaceInterativa interfaceInterativa) {
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.interfaceInterativa = interfaceInterativa;
        this.semaforo = new Semaphore(0);
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {

            String nomePagina = pagina.getNumero().toString();
            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FifoInterativo.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    randLog(Mediador.MISS, nomePagina);
                    lista.insereNoInicio(pagina);
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    randLog(Mediador.INSERIDA, nomePagina);
                    //Hit, página se encontra na lista(memória). 
                } else {
                    randLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    randLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;
                    
                    //Miss, página não se encontra na lista(memória).
                } else {
                    randLog(Mediador.MISS, nomePagina);
                    numeroDeMiss++;
                    Pagina paginaRemovida = lista.randPageFault(pagina, quantidadeDeFrames);
                    randLog(Mediador.REMOVIDA, paginaRemovida.toString());
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    randLog(Mediador.INSERIDA, nomePagina);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                }
            }
            interfaceInterativa.removeDaFila();
        }
        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();

    }

    private void randLog(int tipo, String nomePagina) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina
                        + " já se encontra na memória.\n");
                break;
            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina
                        + " não se encontra na memória.\n");
                break;
            case Mediador.INSERIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " adicionada na memória.\n");
                break;

            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " removida da memória por uma escolha aleatória.\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }
}
