/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYDatasetTableModel;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Vana's Figueiredo
 */
public class GerarGrafico extends javax.swing.JFrame implements ChartChangeListener {

    private JFreeChart chart;
//    private XYDatasetTableModel table;
//    private XYDataset tableDataSet;
    private XYSeriesCollection dataset = new XYSeriesCollection();
    private final XYSeries fifo = new XYSeries("Fifo");
    private final XYSeries lfu = new XYSeries("LfU");
    private final XYSeries mfu = new XYSeries("Mfu");
    private final XYSeries lru = new XYSeries("Lru");
    private final XYSeries nru = new XYSeries("Nur");
    private final XYSeries otimo = new XYSeries("Otimo");
    private final XYSeries rand = new XYSeries("Rand");
    private final XYSeries segChance = new XYSeries("2ª Chance");
    private DemoTableModel model;
    private Home home;
    JTable jtable;

    /**
     * Creates new form gerarGrafico
     */
    public GerarGrafico(Home home) {
        super("Grafico");
        createDatset();
        this.home = home;
        gerarGrafico();
//        tblTabela.setModel(new DemoTableModel(5));
    }

    public void gerarGrafico() {
        chart = ChartFactory.createXYLineChart("Algoritmos de substituicao de páginas", "Quantidade de frames", "Quantidade de acertos", dataset);
//        this.add(new ChartPanel(chart));
//        this.pack();
        chart.addChangeListener(this);
        JPanel panel = new JPanel(new BorderLayout());
        ChartPanel chartPanel = new ChartPanel(chart);

        chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(600, 270));
        chartPanel.setDomainZoomable(true);
        chartPanel.setRangeZoomable(true);
        javax.swing.border.CompoundBorder compoundborder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4), BorderFactory.createEtchedBorder());
        chartPanel.setBorder(compoundborder);
        panel.add(chartPanel);
        model = new DemoTableModel(home.getNumFrameFinal()-home.getNumFrameInicial()+1);
        
        int j = 0;
        for (int i = home.getNumFrameInicial(); i <= home.getNumFrameFinal(); i++) {
            model.setValueAt(i, j, 0);
            j++;
        }
       
        jtable = new JTable(model);
        JPanel jpanel1 = new JPanel(new BorderLayout());
        jpanel1.setPreferredSize(new Dimension(400, 120));
        jpanel1.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));
        jpanel1.add(new JScrollPane(jtable));
        panel.add(jpanel1, "South");
        this.add(panel);
        this.pack();
    }

    @Override
    public void chartChanged(ChartChangeEvent cce) {
        if (chart != null) {
            JFreeChart jfreechart = chart;
            if (jfreechart != null) {
                
                List<XYDataItem> listfifo = new ArrayList();
                listfifo = fifo.getItems();
                
                
                for (XYDataItem item : listfifo) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 1);
                }
                jtable.repaint();
                
                List<XYDataItem> listSegChance = new ArrayList();
                listSegChance = segChance.getItems();
                
                
                for (XYDataItem item : listSegChance) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 2);
                }
                jtable.repaint();
                
                List<XYDataItem> listLFU = new ArrayList();
                listLFU = lfu.getItems();
                
                
                for (XYDataItem item : listLFU) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 3);
                }
                jtable.repaint();
                
                List<XYDataItem> listMFU = new ArrayList();
                listMFU = mfu.getItems();
                
                
                for (XYDataItem item : listMFU) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 4);
                }
                jtable.repaint();
                
                List<XYDataItem> listRand = new ArrayList();
                listRand = rand.getItems();
                
                for (XYDataItem item : listRand) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 5);
                }
                jtable.repaint();
                
                List<XYDataItem> listLRU = new ArrayList();
                listLRU = lru.getItems();
                
                for (XYDataItem item : listLRU) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 6);
                }
                jtable.repaint();
                
                List<XYDataItem> listNUR = new ArrayList();
                listNUR = nru.getItems();
                
                for (XYDataItem item : listNUR) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 7);
                }
                jtable.repaint();
                
                List<XYDataItem> listOtimo = new ArrayList();
                listOtimo = otimo.getItems();
                
                for (XYDataItem item : listOtimo) {
                    int i = 0;
                    while (Double.parseDouble(model.getValueAt(i, 0).toString()) != item.getXValue()) {                        
                        i++;
                    }
                    model.setValueAt(item.getYValue(), i, 8);
                }
                jtable.repaint();
                //for (int i = 0; i < 10; i++) {
//                    table.setValueAt(fifo.getY(0), 0, 0);

               // }

            }
        }
    }

    public synchronized void addFifo(int quantidadeDeFrames, int numeroDeAcertos) {
        fifo.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addLfu(int quantidadeDeFrames, int numeroDeAcertos) {
        lfu.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addMfu(int quantidadeDeFrames, int numeroDeAcertos) {
        mfu.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addLru(int quantidadeDeFrames, int numeroDeAcertos) {
        lru.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addNru(int quantidadeDeFrames, int numeroDeAcertos) {
        nru.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addOtimo(int quantidadeDeFrames, int numeroDeAcertos) {
        otimo.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addRand(int quantidadeDeFrames, int numeroDeAcertos) {
        rand.add(quantidadeDeFrames, numeroDeAcertos);
    }

    public synchronized void addSegChance(int quantidadeDeFrames, int numeroDeAcertos) {
        segChance.add(quantidadeDeFrames, numeroDeAcertos);
    }

    //   private TableXYDataset createTableset() {
    //       tableDataSet.getSeriesCount();
//        dataset.addSeries(lfu);
//        dataset.addSeries(mfu);
//        dataset.addSeries(lru);
//        dataset.addSeries(nru);
//        dataset.addSeries(otimo);
//        dataset.addSeries(rand);
//        dataset.addSeries(segChance);
    //   return tableDataSet;
//}   
    private XYDataset createDatset() {
        dataset.addSeries(fifo);
        dataset.addSeries(lfu);
        dataset.addSeries(mfu);
        dataset.addSeries(lru);
        dataset.addSeries(nru);
        dataset.addSeries(otimo);
        dataset.addSeries(rand);
        dataset.addSeries(segChance);

        return dataset;
    }

    static class DemoTableModel extends AbstractTableModel
            implements TableModel {

        private Object data[][];

        public int getColumnCount() {
            return 9;
        }

        public int getRowCount() {
            return data.length;
        }

        public Object getValueAt(int i, int j) {
            return data[i][j];
        }

        public void setValueAt(Object obj, int i, int j) {
            data[i][j] = obj;
            //    fireTableDataChanged();
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0: // '\0'
                    return "NºFrames/Algoritmo";

                case 1: // '\001'
                    return "FIFO";

                case 2: // '\002'
                    return "Seg.Chance";

                case 3: // '\003'
                    return "LFU";

                case 4: // '\004'
                    return "MFU";

                case 5: // '\005'
                    return "Rand";

                case 6: // '\006'
                    return "LRU";

                case 7: // '\007'
                    return "NUR";

                case 8: // '\008'
                    return "Otimo";
            }
            return null;
        }

        public DemoTableModel(int i) {
            data = new Object[i][9];
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 631, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 476, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
