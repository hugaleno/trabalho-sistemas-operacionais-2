/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.*;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class SegundaChanceInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private List<Pagina> paginas = new ArrayList<>();
    private final int tempoZeresima;
    private int tempoZeresimaDinamico;
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private final Semaphore semaforo;
    private boolean isPassoAPasso = true;
    private final InterfaceInterativa interfaceInterativa;

    public SegundaChanceInterativo(List<Pagina> listaDePaginas,
            int quantidadeDeFrames, int tempo,
            InterfaceInterativa interfaceInterativa) {

        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.tempoZeresima = tempo;
        this.tempoZeresimaDinamico = tempo;
        this.interfaceInterativa = interfaceInterativa;
        this.semaforo = new Semaphore(0);
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            String nomePagina = pagina.getNumero().toString();
            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SegundaChanceInterativo.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            pagina.setReferenciado(true);

            //Simulação da zerésima do sistema operacional. Quando o número de páginas
            //acessadas chega a tempoZeresima, o bit R de todas as páginas da memória
            //são zerados.
            if (tempoZeresimaDinamico == 0) {
                tempoZeresimaDinamico = tempoZeresima;
                lista.zeresima();
                interfaceInterativa.adicionarInfo("Bit de referência zerado "
                        + "para todas as páginas\n");
            }
            tempoZeresimaDinamico--;
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    segundaChanceLog(Mediador.MISS, nomePagina);
                    lista.insereNoInicio(pagina.clone());
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);

                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    segundaChanceLog(Mediador.PAG_REFERENCIADA, nomePagina);
                    segundaChanceLog(Mediador.INSERIDA, nomePagina);
                //Hit, página se encontra na lista(memória).    
                } else {
                    numeroDeHits++;
                    segundaChanceLog(Mediador.HIT, nomePagina);
                    lista.setBitR(pagina);
                    segundaChanceLog(Mediador.PAG_REFERENCIADA, nomePagina);

                }
            } else {
                //Hit, página se encontra na lista(memória).    
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;
                    segundaChanceLog(Mediador.HIT, nomePagina);
                    lista.setBitR(pagina);
                    segundaChanceLog(Mediador.PAG_REFERENCIADA, nomePagina);
                    
                    //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    segundaChanceLog(Mediador.MISS, nomePagina);

                    Pagina paginaRemovida = lista.secondChancePageFault(pagina.clone());
                    segundaChanceLog(Mediador.REMOVIDA, paginaRemovida.toString());
                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    segundaChanceLog(Mediador.PAG_REFERENCIADA, nomePagina);
                    segundaChanceLog(Mediador.INSERIDA, nomePagina);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);

                }
            }
            interfaceInterativa.removeDaFila();
        }

        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();

    }

    private void segundaChanceLog(int tipo, String nomePagina) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina
                        + " já se encontra na memória.\n");
                break;

            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina
                        + " não se encontra na memória.\n");
                break;

            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " removida da memória pois é a última da fila e "
                        + "não foi referenciada recentemente.\n");
                break;

            case Mediador.PAG_REFERENCIADA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " marcada como referenciada.\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }
}
