/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author Hugaleno
 */
public class Otimo extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final GerarGrafico grafico;
    private int quantidadeDeFrames = 0;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;

    public Otimo(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames, GerarGrafico grafico) {
        this.grafico = grafico;
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    public int usadaMaisTarde(int posicaoInicial) {
        return 0;
    }

    @Override
    public void run() {
        HashMap<Pagina, LinkedList<Integer>> map = new HashMap<>();
                //Cria um Hashmap contendo cada página com suas respectivas
        //posições na lista de páginas. 
        for (int i = 0; i < paginas.size(); i++) {
            Pagina key = paginas.get(i);

            if (!map.containsKey(key)) {
                LinkedList list = new LinkedList<>();
                list.add(i);
                map.put(key, list);
            } else {
                map.get(key).add(i);
            }

        }
        for (Pagina pagina : paginas) {
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina);
                //Hit, página se encontra na lista(memória). 
                } else {
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;
                    
                //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    
                    //Checa qual das páginas na memória vai demorar mais a ser
                    //referenciada novamente, ou se ela não será mais referenciada.
                    //Remove da memória a página que não será mais referenciada
                    //ou a que mais demorará a ser referenciada.
                    Pagina key = null;
                    int maior = 0;
                    for (int i = 0; i < lista.numElem(); i++) {
                        if (map.get(lista.getDaPosicao(i)).isEmpty()) {
                            key = lista.getDaPosicao(i);
                            map.remove(key);
                            break;
                        } else {
                            if (maior < map.get(lista.getDaPosicao(i)).getFirst()) {
                                maior = map.get(lista.getDaPosicao(i)).getFirst();
                                key = lista.getDaPosicao(i);
                            }

                        }

                    }

                    lista.otimoPageFault(pagina, lista.getPosicao(key));

                }
            }
            map.get(pagina).poll();

        }
        grafico.addOtimo(quantidadeDeFrames, numeroDeHits);

    }
}
