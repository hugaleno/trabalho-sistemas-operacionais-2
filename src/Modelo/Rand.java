/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;

/**
 *
 * @author Hugaleno
 */
public class Rand extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final GerarGrafico grafico;
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;

    public Rand(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames, GerarGrafico grafico) {
        this.grafico = grafico;
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;

    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina);

                    //Hit, página se encontra na lista(memória).
                } else {
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória).
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;

                    //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    lista.randPageFault(pagina, quantidadeDeFrames);
                }
            }
        }

        grafico.addRand(quantidadeDeFrames, numeroDeHits);

    }
}
