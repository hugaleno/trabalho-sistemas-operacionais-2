/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hugaleno
 */
public class Fifo extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private final GerarGrafico grafico;
    private List<Pagina> paginas = new ArrayList<>();
    private int quantidadeDeFrames = 0;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    

    public Fifo(List<Pagina> listaDePaginas, int quantidadeDeFrames, GerarGrafico grafico) {
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.grafico = grafico;
        
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina);
                //Hit, página se encontra na lista(memória). 
                } else {
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;
                //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    lista.fifoPageFault(pagina);
                }
            }

        }
       
            grafico.addFifo(quantidadeDeFrames, numeroDeHits);
        
        

    }
}
