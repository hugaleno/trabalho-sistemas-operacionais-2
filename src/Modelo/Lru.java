/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Visao.GerarGrafico;
import java.util.ArrayList;

/**
 *
 * @author Hugaleno
 */
public class Lru extends Thread {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private final GerarGrafico grafico;
    private final int quantidadeDeFrames;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;

    public Lru(ArrayList<Pagina> listaDePaginas, int quantidadeDeFrames, GerarGrafico grafico) {
        this.grafico = grafico;
        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void run() {
        for (Pagina pagina : paginas) {
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    lista.insereNoInicio(pagina);
                    //Hit, página se encontra na lista(memória).    
                } else {
                    lista.lruHit(pagina);
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória).
                if (lista.estaNaLista(pagina)) {
                    numeroDeHits++;
                    lista.lruHit(pagina);

                    //Miss, página não se encontra na lista(memória).
                } else {
                    numeroDeMiss++;
                    lista.lruPageFault(pagina);
                }
            }
        }

        grafico.addLru(quantidadeDeFrames, numeroDeHits);

    }
}
