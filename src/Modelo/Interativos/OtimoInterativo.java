/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Interativos;

import Controle.Mediador;
import Controle.Mediador.Interativo;
import Modelo.*;
import Visao.InterfaceInterativa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugaleno
 */
public class OtimoInterativo extends Thread implements Interativo {

    private final ListaEncadeada lista = new ListaEncadeada();
    private ArrayList<Pagina> paginas = new ArrayList<>();
    private int quantidadeDeFrames = 0;
    private int numeroDeMiss = 0;
    private int numeroDeHits = 0;
    private boolean isPassoAPasso = true;
    private final InterfaceInterativa interfaceInterativa;
    private final Semaphore semaforo;

    public OtimoInterativo(ArrayList<Pagina> listaDePaginas,
            int quantidadeDeFrames, InterfaceInterativa interfaceInterativa) {

        this.paginas = listaDePaginas;
        this.quantidadeDeFrames = quantidadeDeFrames;
        this.interfaceInterativa = interfaceInterativa;
        this.semaforo = new Semaphore(0);

    }

    public int getNumeroDeMiss() {
        return numeroDeMiss;
    }

    public int getNumeroDeHits() {
        return numeroDeHits;
    }

    @Override
    public void setIsPassoAPasso(boolean isPassoAPasso) {
        this.isPassoAPasso = isPassoAPasso;
    }

    @Override
    public void run() {
        HashMap<Pagina, LinkedList<Integer>> map = new HashMap<>();
        //Cria um Hashmap contendo cada página com suas respectivas
        //posições na lista de páginas. 
        for (int i = 0; i < paginas.size(); i++) {
            Pagina key = paginas.get(i);

            if (!map.containsKey(key)) {
                LinkedList list = new LinkedList<>();
                list.add(i);
                map.put(key, list);
            } else {
                map.get(key).add(i);
            }

        }
        for (Pagina pagina : paginas) {

            String nomePagina = pagina.getNumero().toString();
            try {
                if (isPassoAPasso) {
                    semaforo.acquire();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FifoInterativo.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            //Enquanto a memória não estiver cheia, nenhuma página precisa ser removida.
            if (lista.numElem() < quantidadeDeFrames) {
                //Miss, página não se encontra na lista(memória).
                if (!lista.estaNaLista(pagina)) {
                    numeroDeMiss++;
                    otimoLog(Mediador.MISS, nomePagina);
                    lista.insereNoInicio(pagina);

                    interfaceInterativa.removePaginaFrame(quantidadeDeFrames);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                    otimoLog(Mediador.INSERIDA, nomePagina);
                    //Hit, página se encontra na lista(memória).     
                } else {
                    otimoLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;
                }
            } else {
                //Hit, página se encontra na lista(memória). 
                if (lista.estaNaLista(pagina)) {
                    otimoLog(Mediador.HIT, nomePagina);
                    numeroDeHits++;
                    //Miss, página não se encontra na lista(memória).    
                } else {
                    otimoLog(Mediador.MISS, nomePagina);
                    numeroDeMiss++;

                    //Checa qual das páginas na memória vai demorar mais a ser
                    //referenciada novamente, ou se ela não será mais referenciada.
                    //Remove da memória a página que não será mais referenciada
                    //ou a que mais demorará a ser referenciada.
                    Pagina key = null;
                    int maior = 0;
                    for (int i = 0; i < lista.numElem(); i++) {
                        if (map.get(lista.getDaPosicao(i)).isEmpty()) {
                            key = lista.getDaPosicao(i);
                            map.remove(key);
                            break;
                        } else {
                            if (maior < map.get(lista.getDaPosicao(i)).getFirst()) {
                                maior = map.get(lista.getDaPosicao(i)).getFirst();
                                key = lista.getDaPosicao(i);
                            }

                        }

                    }

                    Pagina paginaRemovida = lista.otimoPageFault(
                            pagina, lista.getPosicao(key));
                    otimoLog(Mediador.REMOVIDA, paginaRemovida.toString());

                    interfaceInterativa.removePaginaFramePorNome(
                            paginaRemovida.getNumero().toString());

                    otimoLog(Mediador.INSERIDA, nomePagina);
                    interfaceInterativa.inserirPaginaFrame(nomePagina);
                }
            }
            map.get(pagina).poll();
            interfaceInterativa.removeDaFila();
        }
        interfaceInterativa.adicionarInfo("Simulação finalizada.\n");
        interfaceInterativa.reset();
    }

    private void otimoLog(int tipo, String nomePagina) {
        switch (tipo) {
            case Mediador.HIT:
                interfaceInterativa.adicionarInfo("Hit: Página " + nomePagina
                        + " já se encontra na memória.\n");
                break;
            case Mediador.MISS:
                interfaceInterativa.adicionarInfo("Miss: Página " + nomePagina
                        + " não se encontra na memória.\n");
                break;
            case Mediador.INSERIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " adicionada na memória.\n");
                break;

            case Mediador.REMOVIDA:
                interfaceInterativa.adicionarInfo("Página " + nomePagina
                        + " removida da memória pois é página que mais vai "
                        + "demorar a ser referenciada novamente, "
                        + "ou não será mais referenciada no futuro.\n");
                break;

            default:
                interfaceInterativa.adicionarInfo("Tipo desconhecido.\n");
                break;

        }
    }

    @Override
    public void liberaPasso() {
        this.semaforo.release();
    }

}
